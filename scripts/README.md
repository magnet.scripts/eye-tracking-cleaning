# MAGNET Eye Tracking Cleaning Scripts



## VGS and Anti-Saccade
For both VGS and anti-saccade tests, the ‘vgs_cleaner.R’ file can be used to process raw .edf files or readable .asc files. The processing will take the raw data and produce a log of trials that did not meet set criteria and a set of scores for those that did.

### Basic Steps
Once the settings at the top of the script are adjusted to what you need, you can just run the script. 

Most of the settings don’t need to be changed each time. There is a section on the criteria below if you need further information on how to calibrate the settings. If you want to use the existing settings and just update the folders and files, all you need to do is tell the script;
1.	Where to look for the raw data (by updating input_folder_path) 
2.	Whether the raw .edf files need to be converted or you already have .asc files (set convert_edf_required to TRUE/FALSE)
3.	Where you want the output saved (by updating output_folder_path)
4.	Whether you want it run for anti-saccade or saccade (set antisaccade_target to TRUE/FALSE)

Once you are happy with those (and the other criteria match the defaults or your custom criteria), the script can be run.

The script can be run from an IDE (e.g. RStudio) or from the command line (e.g. Rscript S:\path_to_script\ vgs_cleaner.R). 

Note: Conversion from edf to asc relies on Eyelink Developers Kit from SR Research being installed (with the edf2asc tool on the PATH of the environment where the R script is run). 

### Output
The script creates two data files: the main summary scores output and a log of the cleaning. The main output is participant level data from the cleaned eye-tracking. The cleaning log file records all breaches of criteria found along with notes about that breach. To find participants that were excluded by the script, filter the ‘block’ column to -1 or the category to ‘participant_excluded’.

As there are two levels of criteria (main criteria and borderline cases), the cleaning log also indicates whether or not the breach met the borderline criteria and whether or not it was included (which depends on include_borderline_cases; see criteria section below).

Visualisations can be included in the output if required (see visualisation section below).

### Dependencies
The script is written in R programming language and requires that R be installed on the computer to run. Three R libraries are also needed: eyelinker, dplyr, and ggplot (if creating plots).

Conversion from edf to asc relies on the Eyelink Developers Kit from SR Research being installed (so the command line tool edf2asc is available and on the PATH of the environment where the R script is run).

### Assumptions on Experimental Design
This script works for the MAGNET eye tracking experiment at the time of writing by using several defaults of SR Research Experiment Builder and Eyelink 1000 plus. It may not generalise to other methods of gathering visually guided saccade data. 

The main assumptions are that data in the edf/asc will be for a single participant, split into blocks with each block having a central fix followed by single, immediate target relocation to a different spot on the horizontal while staying the same vertically. It also currently relies on there being only one target object and Experiment Builder default messages about targets in the edf.

The script may need to be modified to overcome these assumptions. There are also three variables in relation to the experiment design that are hard coded (screen_height, screen_width, screen_dist) that may also need to be adjusted for different set ups.

### Criteria for Cleaning
The following parameters can be adjusted to different thresholds to change the criteria for exclusion or inclusion in the output. The defaults reflect the values used to best reflect the manual cleaning that had been done for MAGNET prior to this script being written.

Main criteria:

1.	exclude_direction_err (default:  TRUE):  Exclude if first saccade wrong direction
2.	min_total_trials (default:  6): Minimum count where all data from edf is excluded if fewer trials were acceptable
3.	include_borderline_cases (default:  TRUE): If TRUE includes borderline cases when standard criteria exclude the trial, if FALSE identifies the borderline cases but still excludes the trial

Criteria with potential for borderline inclusion (i.e. if borderline criteria are met and include_borderline_cases is TRUE):

4.	min_central_fix (default:  500): Time in ms for fixation on central point before eye movement 
5.	bl_min_central_fix (default:  300): Borderline time in ms for fixation on central point before eye movement 
6.	min_time_after_target	(default:  100):	Minimum time in ms after target is presented for valid (not pre-emptive) movement
7.	bl_time_after_target (default:  80):  Borderline time in ms after target is presented for valid (not preemptive) movement
8.	blink_free_pre (default:  500): Time in ms with no blinks before target moves from centre (set to -1 to skip blink checks)
9.	bl_blink_free_pre (default:  300): Borderline time in ms with no blinks before target moves from centre 
10.	blink_free_post (default:  100): Time in ms with no blinks after target moves from centre 
11.	bl_blink_free_post (default:  80): Borderline time in ms with no blinks after target moves from centre 
12.	first_sac_min (default:  3): Minimum size in degrees of first saccade towards target
13.	bl_first_sac_min (default: 2.5 ): Borderline size of first saccade towards target
14.	final_fix_pos (default:  3): max difference in degrees between targ_amp (size of target move) and fix_amp (size of difference between starting fixation and final fixation - the max fixation less than this will be used as final fix) 
15.	bl_final_fix_pos (default:  3.5 ):  borderline max difference in degrees between targ_amp (size of target move) and fix_amp (size of difference between starting fixation and final fixation - the max fixation less than this will be used as final fix) 

Criteria about the nature of the central fixation:

16.	unbroken_srr_fixation (default:  TRUE): If TRUE, checks whether the SR Research fixations were unbroken from the min_central_fix before the target moved until the first sacc (set as FALSE to skip)
17.	central_fix_clusters (default:  1): Max size in degrees of shift in fixation location to accept during lead in (from centre of eye movement in lead in)  central fixation (set to -1 to skip)
18.	allowable_sac (default: .25 ):  if filtering small saccades from consideration in lead in period (otherwise, set to 0 to consider all sacs or -1 or less to skip this check)
19.	max_central_dist (default:  -1):  Max distance from central point during lead in (set to -1 to use unlimited)

### Visualisations
The script can provide visualisations of trials or summary visualisations of for each participant. Simply toggle create_charts TRUE/FALSE and choose the number of files (nFiles_vis) to visualise and the number of trials per file (nTrials_vis). The script will then randomly sample the index numbers for both files and trials in the quantity you specify. Selecting more that the available files/trials will result in all available being created (e.g. can set both to Inf to get all charts for all files).

Alternatively, you can specify the index location of files (from_file_index) and trials (from_trial_index) to return consistent, specific charts by adding a vector of the indexes required (e.g. “from_file_index <- c(1, 5, 10, 11)”) . If either of these variables are anything other than an empty vector (c()) it will override the random selection of indexes.

Visualisations will be saved into the output folder with the output file and cleaning log.

## Smooth Pursuit
As with the VGS script, the ‘spem_cleaner.R’ file can be used to process raw .edf files or readable .asc files. The processing will take the raw data and produce a log of trials that did not meet set criteria and a set of scores for those that did.

### Basic Steps
Once the settings at the top of the script are adjusted to what you need, you can just run the script. 

Most of the settings don’t need to be changed each time. There is a section on the criteria below if you need further information on how to calibrate the settings. If you want to use the existing settings and just change the folders and files, all you need to do is tell the script;

1.	Where to look for the raw data (by updating input_folder_path) 
2.	Whether the raw .edf files need to be converted or you already have .asc files (set convert_edf_required to TRUE/FALSE)
3.	Where you want the output saved (by updating output_folder_path)

Once you are happy with those (and the other criteria match the defaults or your custom criteria), the script can be run.

The script can be run from an IDE (e.g. RStudio) or from the command line (e.g. Rscript S:\path_to_script\ vgs_score.R). 

Note: Conversion from edf to asc relies on Eyelink Developers Kit from SR Research being installed (with the edf2asc tool on the PATH of the environment where the R script is run). 

### Output
The script creates two data files: the main output and a log of the cleaning. The main output is participant level data from the cleaned eye-tracking. The cleaning log file records all breaches of criteria found along with notes about that breach. To find participants that were excluded by the script, filter the category to ‘participant_excluded’.

Visualisations can be included in the output if required (see visualisation section below).

### Dependencies
The script is written in R programming language and requires that R be installed on the computer to run. Three R libraries are also needed: eyelinker, dplyr, and ggplot (if creating plots).

Conversion from edf to asc relies on the Eyelink Developers Kit from SR Research being installed (so the command line tool edf2asc is available and on the PATH of the environment where the R script is run).

### Assumptions on Experimental Design
This script works for the MAGNET eye tracking experiment at the time of writing by using several defaults of SR Research Experiment Builder and Eyelink 1000 plus. It may not generalise to other methods of gathering visually guided saccade data. 

The main assumptions are that data in the edf/asc will be for a single participant, split into blocks with each block having a central fix followed by single, continuous movement from one side of the screen to the other moving only on the horizontal while staying the same vertically. It also currently relies on there being only one target object and Experiment Builder default messages about targets in the edf.
The script may need to be modified to overcome these assumptions. There are also three variables in relation to the experiment design that are hard coded (screen_height, screen_width, screen_dist) that may also need to be adjusted for different set ups.

### Criteria for Cleaning
The following parameters can be adjusted to different thresholds to change the criteria for exclusion or inclusion in the output. The defaults reflect the values used to best reflect the manual cleaning that had been done for MAGNET prior to this script being written.

Main criteria:

1.	target_proportion (default:  .7): Inner extent of target to take for smooth pursuit (all if 1) to cut off direction change
2.	sacc_buffer  (default:50): ms either side of a saccade (so that sacc velocity does not influence purstuit velocity)
3.	catchup_sacc_prop (default: .5): proportion of saccade that needs to be behind the target in order to class the saccade as catch-up
4.	anticipatory_sacc_prop (default: .5): proportion of saccade that needs to be in front of the target in order to class the saccade as anticipatory
5.	slow_threshold_vel (default: 1): velocity (deg/sec) for slow/no moving eye movement sustained for slow_threshold_time to be excluded
6.	slow_threshold_time (default: 1000): ms for sustained slow/no moving eye movement to be excluded
7.	pusuit_direction_err (default: "all"): to exclude pursuit in the that is not moving in the same direction as the target at all times if 'all' and only opposite direction while trailing behind the target if 'trailing' (skipped for any other value)
8.	delay_nth_target (default: 5): nth different target position in each block to choose as starting point for smooth pursuit being possible
9.	distracted_deg (default: 4): data excluded if average rmse is greater than this while also below the distracted_vel_dif for the distracted_window
10.	distracted_vel_dif (default: .8): data excluded if velocity gain more than +/- this figure from 1 while also beyond the distracted_deg for the distracted_window
11.	distracted_window (default: 1000): data excluded if more than this period passes while the average distracted_deg and distracted_vel_dif are outside their criteria

### Visualisations
The script can provide visualisations of trials. Simply toggle create_charts TRUE/FALSE and choose the number of files (nFiles_vis) to visualise and the number of trials per file (nTrials_vis). The script will then randomly sample the index numbers for both files and trials in the quantity you specify. Selecting more that the available files/trials will result in all available being created (e.g. can set both to Inf to get all charts for all files).

Alternatively, you can specify the index location of files (from_file_index) and trials (from_trial_index) to return consistent, specific charts by adding a vector of the indexes required (e.g. “from_file_index <- c(1, 5, 10, 11)”) . If either of these variables are anything other than an empty vector (c()) it will override the random selection of indexes.

Visualisations will be saved into the output folder with the output file and cleaning log.


