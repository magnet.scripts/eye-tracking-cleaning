# Eye Tracking Cleaning (SacLab)
## Prosaccade, Antisaccade, and Smooth Pursuit Tasks
SacLab (the R-Shiny app) can be used to automatically process data in bulk from standard prosaccade, antisaccade, and smooth pursuit tasks recorded via SR Research EyeLink. It returns a range of useful statistics per trial and per participant or file. It is made available under a GNU General Public License in the hopes that it can be further improved and expanded to meet a broader range of needs.

## Basic Steps
1. R programming language and the shiny library need to be installed on a computer (using r-studio as an IDE for R which comes with shiny pre-packaged is recommended)

2. Download the app.R file (from: https://gitlab.com/magnet.scripts/eye-tracking-cleaning)

3. Get the EDF2ASC conversion tool from the SR Research support pages and convert all EDFs to ASC

4. Run the app (in R-studio, open the file and click 'Run App')

5. Upload the ASC files.

6. Change the settings (note particularly the data splitter and physical setup settings).

7. Click 'Run and download output' and be prepared to wait a while (~10-20 minutes depending on the system, settings, and the amount of data) for the download.

## Output
The script creates three main files: 
- Cleaning log - with all the details of what was excluded and why
- Trial data - the trial by trial output for each participant
- Output - the per participant summary statistics

### Eye comparison
The app also has the option to compare the left and the right eye in the eye-tracking recording (by checking "Compare left and right eyes" in the Output tab). If checked, the 3 above files will all have entries for both left and right eyes. There will also be an additional 'best_eye' file. This will select each trial based on which eye meets the cleaning criteria, and then has the lesser value for the 'Eye selection metric' criteria, and then the 'Default eye'.

### Visualisations
Visualisations can be included in the output if required. The app can provide visualisations of trials or summary visualisations for each participant. Simply toggle 'Include Charts' and then choose the number of files to visualise and the number of trials per file to get a random sampling of files and trials. If a number greater than the number of files/trials is specified, all available will be visualised.

Alternatively, you can check 'Charts for Specific Files/Trials (by index)' to enter the specify the index location of files and trials to return consistent, specific charts

## Assumptions
The app can be set to suit different cleaning requirements and experimental configurations as long as some basic assumptions are met. 

### Data Format
The data needs to be in ASC format (converted from EDF using SR Research EDF2ASC Conversion Utility) resulting from tracking from an EyeLink (e.g. EyeLink II, EyeLink 1000, or EyeLink 1000 plus). 

If the data is still in EDF format and the command line edf2asc tool is available on the PATH, then the original script versions (in the scripts folder) can be used to both convert to ASC and clean the data as part of a single process.

### Experimental Design
#### Prosaccade and Antisaccade
The main assumptions are that data in the ASC will be for a single participant, split into blocks with each block having a central fix followed by single, immediate target relocation to a different spot on the horizontal while staying the same vertically. In prosaccade, the participant is to aim to look where the target is located. In antisaccade, the structure is the same but the participant is expected to look at a position in the mirrored location on the screen.

To use the 'SR Research Default' in the 'Data Splitter' selector, it also relies on there being only one target object and Experiment Builder default messages about targets in the EDF. Other options in the 'Data Splitter' have been designed for specific experimental set ups and an additional loader that identifies the target in the data may be required if Experiment Builder defaults that allow the target to be seen in the DataViewer are not used.

#### Smooth Pursuit
Again, the main assumptions are that data in each ASC file will be for a single participant, split into blocks with each block having a central fixation followed by single, continuous movement from one side of the screen to the other moving only on the horizontal while staying the same vertically. 

It also currently relies on there being only one target object and Experiment Builder default messages about targets in the ASC file.
The app may need to be modified to overcome these assumptions for other set ups. 

## Dependencies
The script is written in R programming language and requires that R be installed on the computer to run. Some R libraries are also needed: shiny, eyelinker, dplyr, stringr, zip, signal, and ggplot.

## Saving Settings
Most of the settings don’t need to be changed each time. Once set, a save file for the settings can be created and then uploaded to reset to those values. The settings save file only relates to the settings (no data included).

## Configuration of Settings
### Main Tab
The first radio buttons on this tab allow you to select the type of task; Prosaccade, Antisaccade, or Smooth Pursuit. While Prosaccade and Antisaccade options are the same, changing to smooth pursuit will change the available cleaning options.

 - **Data Splitter** - the method for finding the target in the data. SR Research Default will work if the target was set in Experiment Builder in a way that allows the target to be viewed in the DataViewer.
 - **Screen Height** - set the height of the screen in millimetres
 - **Screen Width** - set the width of the screen in millimetres
 - **Screen to participant** - set the distance between the screen and the participant in millimetres

### Reprocess Tab
 - **Default eye** - select the default eye that will be used either in all cases where available or where it is comparing eyes but not 'best eye' can be established using the 'Eye Selection Metric'
 - **Apply DSP filter to raw data** - Check this to apply a filter to the raw data. Once checked the following options will be visible for the filter.
    - **Use raw data where filter corrects over threshold** - If checked, you can see and set the **Degree threshold for filter correction**. If checked and the filter corrects away from the raw underlying data to an extent greater than the threshold at any given point, it will default back to the raw data value at that point. 
    - **Filter type** - currently only Butterworth filter is available
    - **Butterworth Type** - select type of Butterworth. Default is low (for low pass). See signal library for more detail.
    - **Frequency cutoff for higher/lower** - Normalised frequency (frequency/nyquist) for cutoff value. Defaults to 0.12 (30Hz/250)
    - **Order of filter** - lower numbers providing steeper transition at edge of filtered range. See signal library for more detail.
 - **Reprocess saccades and fixations from raw/filtered data** - check this to reprocess the saccades and fixations from the raw processing (e.g. as required after filtering raw data). Once checked the following options will be visible for the filter.
     - **Velocity threshold** - saccade velocity threshold
     - **Amplitude threshold** - saccade amplitude threshold
     - **Acceleration threshold** - saccade acceleration threshold
     - **Minimum sample count** - number of consecutive samples required to verify event (saccade/fixation) in data

### Cleaning Tab
The cleaning tab options are for the most part only applicable to the saccade tasks or smooth pursuit tasks. The only exception is:
 - **Min. trials per participant** - files will be excluded if there are fewer than this number of trials that meet the cleaning criteria.

#### Options for cleaning the saccade tasks (prosaccade and antisaccade)
 - **Include borderline cases** - If checked includes borderline cases when standard criteria exclude the trial. If not checked, identifies the borderline cases but still excludes the trial.
 - **Min. central fix** - Time in ms for fixation on central point before eye movement
 - **Borderline min. central fix** - Borderline time in ms for fixation on central point before eye movement
 - **Min. time after target** - Minimum time in ms after target is presented for valid (not pre-emptive) movement
 - **Borderline min. time after target** - Borderline time in ms after target is presented for valid (not pre-emptive) movement
- **Min. blink free before target** - Time in ms with no blinks before target moves from centre (set to -1 to skip blink checks)
- **Borderline min. blink free before target** - Borderline time in ms with no blinks before target moves from centre 
- **Min. blink free after target** - Time in ms with no blinks after target moves from centre
- **Borderline min. blink free after target** - Borderline time in ms with no blinks after target moves from centre
- **Min. first sacc** - Minimum size in degrees of first saccade towards target
- **Borderline min. first sacc** - Borderline size of first saccade towards target
- **Final fix position by proportion of target** - If checked, 'Final fix position' will be proportion of target in that trial. If unchecked, 'Final fix position' is in degrees 
- **Final fix position** - max difference (in degrees or proportion of target, depending on 'Final fix position by proportion of target') between target amplitude (size of target move) and fixation amplitude (size of difference between starting fixation and fixation after first saccade - the longest fixation less than this threshold will be used as final fixation)
- **Borderline final fix position** - borderline max difference (in degrees or proportion of target, depending on 'Final fix position by proportion of target') between target amplitude (size of target move) and fixation amplitude (size of difference between starting fixation and fixation after first saccade - the longest fixation less than this threshold will be used as final fixation if it is a borderline trial)
- **First sacc error distance** - Distance at end of first saccade to target (set to -1 to skip this check)
- **Borderline first sacc error distance** - Borderline distance at end of first saccade to target 
- **Drift during baseline** - Max size in degrees of shift in fixation location to accept during lead in (from centre of eye movement in lead in)  central fixation (set to -1 to skip)
- **Sacc during baseline** - proportion of target size under which saccades are acceptable during baseline lead in period (otherwise, set to 0 to filter any saccade or -1 to skip saccade checks)
- **Max distance from centre** - Max distance from central point during lead in (set to -1 to use unlimited)
- **Unbroken SR Research fixation** - If ticked, checks whether the  fixations were unbroken from the 'Min. central fix' time before the target moved until the first saccade (check is skipped if unticked)
- **Exclude direction errors** - Exclude trial if first saccade is in wrong direction
- **Ignore y-axis movement** - Ignores vertical differences in all movement and velocity calculations

#### Options for cleaning the smooth pursuit task
 - **Inner proportion of target movement to include** - Inner proportion of target movement to take for smooth pursuit (all if 1). To cut off direction change at outer edges
 - **Sacc buffer** - ms either side of a saccade (so that sacc velocity does not influence pursuit velocity)
 - **Catchup sacc proportion in front** - proportion of saccade that needs to be behind the target in order to class the saccade as catch-up
 - **Anticipatory sacc proportion in front** - proportion of saccade that needs to be in front of the target in order to class the saccade as anticipatory
 - **Slow velocity threshold** - velocity (deg/sec) for slow/no moving eye movement sustained for 'Slow time threshold' to be excluded
 - **Slow time threshold** - ms for sustained slow/no moving eye movement to be excluded
 - **Pursuit direction error** - to exclude pursuit in the that is not moving in the same direction as the target at all times if 'all' and only opposite direction while trailing behind the target if 'trailing'. Skipped is 'skipped' (default).
 - **Delay till nth target** - nth different target position in each block to choose as starting point for smooth pursuit being possible
 - **Distracted threshold (deg distance)** - data excluded if average RMSE is greater than this while also below the 'Distracted threshold (vel gain)' for the 'Distracted time window (ms)'
 - **Distracted threshold (vel gain)** - data excluded if velocity gain more than +/- this figure from 1 while also beyond the 'Distracted threshold (deg distance)' for the 'Distracted time window (ms)'
 - **Distracted time window (ms)** - data excluded if more than this period passes while the average 'Distracted threshold (deg distance)' and 'Distracted threshold (vel gain)' are outside their criteria

### Output Tab
- **Split variables by...** - Will provide summary statistics in the output file for specific conditions withing the trials (either summarising by direction and degree for saccade tasks or by velocity in smooth pursuit tasks)
- **Compare left and right eyes** -  If checked, output files will all have entries for both left and right eyes where available. There will also be an additional 'best_eye' file. This will select each trial based on which eye meets the cleaning criteria, and then has the lesser value for the 'Eye selection metric' criteria, and then the 'Default eye'. If unchecked, the default eye will be used for all trials where available.
- **Eye selection metric** -  The metric used to determine the best eye. Can be the standard deviation during the baseline lead in to each task or after the target moves. Alternatively, it can be a count of how often the target move outside the 'Degree threshold for stability counts' in those same periods.
- **Include Charts** -  Whether or not to include visualisations. If selected, will see the following options.
     - **Charts for Specific Files/Trials (by index)** - If checked, provides fields for the index of the files ('**Index of file(s) to plot**') and trials ('**Index of trial(s)**') you wish to visualise. If unchecked, provides fields for the number of files ('**Number of files to create charts for**') and trials ('**Number of trials to chart per file**') - if either of these are greater than what is available, it will output charts for whatever is available.

### Save/Load
By clicking the 'Save Settings' button, a file will be created with all the current settings saved. No eye-tracking data will be saved in this file. This can then be uploaded again via the 'Load Settings' file selector to reinstate those settings.